package webserver;

import nioserver.runners.ServerAcceptorRunner;
import nioserver.runners.ServerProcessRunner;

import javax.inject.Inject;
import java.io.Closeable;
import java.io.IOException;

public class WebServer implements Closeable {
    private ServerAcceptorRunner serverAcceptorRunner;
    private ServerProcessRunner serverProcessRunner;

    @Inject
    public WebServer(ServerAcceptorRunner serverAcceptorRunner, ServerProcessRunner serverProcessRunner)
    {
        this.serverAcceptorRunner = serverAcceptorRunner;
        this.serverProcessRunner = serverProcessRunner;
    }

    public void start() throws IOException {
        new Thread(this.serverAcceptorRunner).start();
        new Thread(this.serverProcessRunner).start();
    }

    @Override
    public void close() throws IOException {
        this.serverAcceptorRunner.stop();
        this.serverProcessRunner.stop();
    }
}
