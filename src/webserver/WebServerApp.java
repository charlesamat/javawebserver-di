package webserver;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.io.IOException;

public class WebServerApp implements Runnable {

    private static final String htDocsPath = System.getProperty("user.dir") + "/htdocs";

    @Inject
    WebServer webServer;

    @Inject
    @Named("port")
    int port;

    @Override
    public void run() {
        try {
            setup();
            webServer.start();
            System.out.println("WebServer is now running on http://localhost:" + port);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

    private static void setup() {
        File directory = new File(htDocsPath);
        if (!directory.exists()) {
            directory.mkdir();
            System.out.println("Created htDocs folder!\r\n Add HTML/CSS/JS files!");
        }
    }
}
