package nioserver.selected;

import nioserver.channelio.OnWrite;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

public class OnWriteSelected extends OnSelected {

    private OnWrite onWrite;

    @Inject
    public OnWriteSelected(@Named("writeSelector") Selector writeSel, OnWrite onWrite) {
        super(writeSel);
        this.onWrite = onWrite;
    }

    @Override
    public void selected(SelectionKey selectionKey) throws IOException {
        onWrite.write((SocketChannel) selectionKey.channel(), (String) selectionKey.attachment());
    }
}
