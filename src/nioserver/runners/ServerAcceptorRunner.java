package nioserver.runners;

import nioserver.acceptors.ServerSocketChannelAcceptor;

import javax.inject.Inject;
import java.nio.channels.SocketChannel;
import java.util.LinkedList;
import java.util.Queue;

public class ServerAcceptorRunner extends Runner{

    private ServerSocketChannelAcceptor serverAcceptor;
    private Queue<SocketChannel> socketChannelQueue;

    @Inject
    public ServerAcceptorRunner(ServerSocketChannelAcceptor serverAcceptor, Queue<SocketChannel> socketChannelQueue) {
        this.serverAcceptor = serverAcceptor;
        this.socketChannelQueue = socketChannelQueue;
    }

    @Override
    public void run() {
        while(running) {
            SocketChannel acceptedChannel = serverAcceptor.accept();
            if(acceptedChannel != null)
                socketChannelQueue.add(acceptedChannel);
            sleep(60);
        }
    }
}
