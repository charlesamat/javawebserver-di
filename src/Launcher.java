import DI.WebServerModule;
import dagger.ObjectGraph;
import tests.TestRunner;
import webserver.WebServerApp;

import java.io.IOException;
import java.util.Scanner;

public class Launcher {

    public static void main(String[] args) throws IOException {
        ObjectGraph objectGraph = ObjectGraph.create(new WebServerModule());
        WebServerApp webServerApp = objectGraph.get(WebServerApp.class);
        webServerApp.run();
    }

    private static void runTests() {
        TestRunner testRunner = new TestRunner();
        testRunner.runTests();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Press enter to exit.");
        scanner.nextLine();
    }
}
