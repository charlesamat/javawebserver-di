package DI;

import dagger.Module;
import dagger.Provides;
import nioserver.channelio.OnRead;
import nioserver.channelio.OnWrite;
import nioserver.channelio.SocketChannelReader;
import nioserver.generators.ServerGenerator;
import nioserver.processors.Processor;
import nioserver.processors.SocketChannelProcessor;
import nioserver.selected.OnReadSelected;
import nioserver.selected.OnSelected;
import nioserver.selected.OnWriteSelected;
import webserver.WebServerApp;
import webserver.http.HTTPRequestProcessor;
import webserver.http.HTTPResponse;
import webserver.server.WebServerGenerator;

import javax.inject.Named;
import javax.inject.Singleton;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.LinkedList;
import java.util.Queue;

@Module(injects = WebServerApp.class)
public class WebServerModule {
    @Provides
    public Processor provideProcessor(SocketChannelProcessor socketChannelProcessor) {
        return socketChannelProcessor;
    }

    @Provides
    public ServerGenerator provideServerGenerator(WebServerGenerator webServerGenerator) {
        return webServerGenerator;
    }

    @Provides
    public ByteBuffer provideByteBuffer() {
        return ByteBuffer.allocate(8192);
    }

    @Provides
    @Named("port")
    public int providePort() {
        return 1337;
    }

    @Provides
    @Named("onRead")
    public OnSelected provideOnReadSelected(OnReadSelected onReadSelected) {
        return onReadSelected;
    }

    @Provides
    @Named("readSelector")
    @Singleton
    public Selector provideReadSelector() {
        try {
            return Selector.open();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Provides
    @Named("onWrite")
    public OnSelected provideOnWriteSelected(OnWriteSelected onWriteSelected) {
        return onWriteSelected;
    }

    @Provides
    @Named("writeSelector")
    @Singleton
    public Selector provideWriteSelector() {
        try {
            return Selector.open();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Provides
    public OnWrite provideOnWrite() {
        return new HTTPResponse(new HTTPRequestProcessor(System.getProperty("user.dir") + "/htdocs"));
    }

    @Provides
    public OnRead provideOnRead(SocketChannelReader socketChannelReader) {
        return socketChannelReader;
    }

    @Provides
    @Singleton
    public Queue<SocketChannel> provideSocketChannelQueue() {
        return new LinkedList<>();
    }
}